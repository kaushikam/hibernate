package com.kaushikam.domain.model.many_to_one.unidirectional;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class FindPostTest {

    @BeforeAll
    public static void setup() {
        Session session = getSession();
        session.beginTransaction();

        Book book = new Book();
        book.setTitle( "High-Performance Java Persistence" );
        book.setAuthor( "Vlad Mihalcea" );
        book.setIsbn( "978-9730228236" );
        session.save(book);

        session.getTransaction().commit();

        session.beginTransaction();
        Publication amazonUs = new Publication();
        amazonUs.setPublisher( "amazon.com" );
        amazonUs.setBook( book );
        amazonUs.setPriceCents( 4599 );
        amazonUs.setCurrency( "$" );
        session.save(amazonUs);
        session.getTransaction().commit();

        session.beginTransaction();
        Publication amazonUk = new Publication();
        amazonUk.setPublisher( "amazon.co.uk" );
        amazonUk.setBook( book );
        amazonUk.setPriceCents( 3545 );
        amazonUk.setCurrency( "&" );
        session.save(amazonUk);
        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testFindPost() {
        Session session = getSession();
        session.beginTransaction();

        Publication publication = session.load(Publication.class, 2L);

        Assertions.assertEquals(2L, publication.getId());
        Assertions.assertEquals("amazon.com", publication.getPublisher());

        session.getTransaction().commit();
        session.close();
    }

    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{Book.class, Publication.class})
                .openSession();
    }
}
