package com.kaushikam.domain.model.many_to_one.unidirectional;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.Session;
import org.junit.jupiter.api.Test;

public class SavePostTest {

    @Test
    public void testSave() {
        Session session = getSession();
        session.beginTransaction();

        Book book = new Book();
        book.setTitle( "High-Performance Java Persistence" );
        book.setAuthor( "Vlad Mihalcea" );
        book.setIsbn( "978-9730228236" );
        session.save(book);

        session.getTransaction().commit();

        session.beginTransaction();
        Publication amazonUs = new Publication();
        amazonUs.setPublisher( "amazon.com" );
        amazonUs.setBook( book );
        amazonUs.setPriceCents( 4599 );
        amazonUs.setCurrency( "$" );
        session.save(amazonUs);
        session.getTransaction().commit();

        session.beginTransaction();
        Publication amazonUk = new Publication();
        amazonUk.setPublisher( "amazon.co.uk" );
        amazonUk.setBook( book );
        amazonUk.setPriceCents( 3545 );
        amazonUk.setCurrency( "&" );
        session.save(amazonUk);
        session.getTransaction().commit();
        session.close();
    }


    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{Book.class, Publication.class})
                .openSession();
    }
}
