package com.kaushikam.domain.model.one_to_one.maps_id_owner_inverse;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class UserFindTest {

    @BeforeAll
    public static void setup() {
        Session session = getSession();
        session.beginTransaction();

        User user = new User();
        user.setUserName("kaushikam");

        Profile profile = new Profile();
        profile.setEmail("kaushikam@gmail.com");

        profile.setUser(user);
        user.setProfile(profile);

        Long id = (Long) session.save(user);

        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testFind() {
        Session session = getSession();
        session.beginTransaction();

        User user = session.load(User.class, 1L);
        Assertions.assertEquals("kaushikam", user.getUserName());

        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testFindProfile() {
        Session session = getSession();
        session.beginTransaction();

        Profile profile = session.load(Profile.class, 1L);
        Assertions.assertEquals("kaushikam@gmail.com", profile.getEmail());

        session.getTransaction().commit();
        session.close();
    }

    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{User.class, Profile.class})
                .openSession();
    }
}
