package com.kaushikam.domain.model.one_to_one.join_table;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class FindEmployeeTest {

    @BeforeAll
    public static void setup() {
        Session session = getSession();
        session.beginTransaction();

        Employee employee = new Employee("Kaushik");

        WorkStation workStation = new WorkStation("2nd Floor");

        employee.setWorkStation(workStation);
        workStation.setEmployee(employee);

        session.save(employee);

        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testFindEmployee() {
        Session session = getSession();
        session.beginTransaction();

        Employee employee = session.load(Employee.class, 1L);

        Assertions.assertEquals("Kaushik", employee.getName());
        Assertions.assertEquals("2nd Floor", employee.getWorkStation().getFloor());

        session.getTransaction().commit();
        session.close();
    }

    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{Employee.class, WorkStation.class})
                .openSession();
    }
}
