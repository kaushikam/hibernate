package com.kaushikam.domain.model.one_to_one.join_column;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PostPersistenceTest {

    @Test
    public void savePost() {
        Session session = getSession();
        session.beginTransaction();

        Post post = new Post();
        post.setTitle("Test title");

        PostDetails postDetails = new PostDetails("Hibernate");

        post.setPostDetails(postDetails);

        Long id = (Long) session.save(post);
        /**
         * Comment following lines to see the that post is not getting inserted.
         * When we call save, only a sequence get executed to fetch the next id
         * (As we have given @GeneratedValue(strategy = GenerationType.AUTO))
         */

        session.getTransaction().commit();
        session.close();

        Assertions.assertNotNull(id);
        Assertions.assertEquals(1L, id);
    }

    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{Post.class, PostDetails.class})
                .openSession();
    }
}
