package com.kaushikam.domain.model.one_to_one.maps_id;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class BookFindTest {

    @BeforeAll
    public static void setup() {
        Session session = getSession();
        session.beginTransaction();

        Book book = new Book();
        book.setTitle("Hibernate Persistence");

        BookInfo bookInfo = new BookInfo();
        bookInfo.setAuthor("John");

        bookInfo.setBook(book);
        book.setBookInfo(bookInfo);

        Long id = (Long) session.save(book);

        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testFindBook() {
        Session session = getSession();
        session.beginTransaction();

        Book book = session.load(Book.class, 1L);
        Assertions.assertEquals("Hibernate Persistence", book.getTitle());

        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testFindBookInfo() {
        Session session = getSession();
        session.beginTransaction();

        BookInfo bookInfo = session.load(BookInfo.class, 1L);
        Assertions.assertEquals("John", bookInfo.getAuthor());
        Assertions.assertEquals(1L, bookInfo.getId());

        session.getTransaction().commit();
        session.close();
    }

    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{Book.class, BookInfo.class})
                .openSession();
    }
}
