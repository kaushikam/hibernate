package com.kaushikam.domain.model.one_to_one.join_column;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.LazyInitializationException;
import org.hibernate.Session;
import org.hibernate.proxy.HibernateProxy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class PostPersistenceFind {

    @BeforeAll
    public static void setup() {
        Session session = getSession();
        session.beginTransaction();

        Post post = new Post();
        post.setTitle("Test title");

        PostDetails postDetails = new PostDetails("Hibernate");

        post.setPostDetails(postDetails);

        Long id = (Long) session.save(post);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * As the session load method returns a HibernateProxy object,
     * without touching the database, after closing the session, if
     * we call any getter methods (Condition: We should not call those methods
     * before closing session) * except the identifier method, we
     * will get a LazyInitializationException.
     */
    @Test
    public void testSessionLoadMethod() {
        Session session = getSession();
        session.beginTransaction();

        Post post = session.load(Post.class, 1L);
        Assertions.assertTrue(isPostAProxy(post));
        session.getTransaction().commit();
        session.close();

        Assertions.assertThrows(LazyInitializationException.class, post::getTitle);
    }

    /**
     * First of all run the following test with fetch type lazy in Post class
     * After that run the test, after removing fetch type lazy (Default type is Eager)
     * and find out that all of the object graph is selected using 'join'(i.e, get all
     * objects including PostDetails
     */
    @Test
    public void testFind() {
        Session session = getSession();
        session.beginTransaction();

        Post post = session.load(Post.class, 1L);

        Assertions.assertEquals("Test title", post.getTitle());

        session.getTransaction().commit();
        session.close();
    }

    private boolean isPostAProxy(Post post) {
        if (HibernateProxy.class.isInstance(post)) {
            HibernateProxy proxy = HibernateProxy.class.cast(post);
            return (proxy.getHibernateLazyInitializer().isUninitialized());
        }

        return false;
    }

    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{Post.class, PostDetails.class})
                .openSession();
    }
}
