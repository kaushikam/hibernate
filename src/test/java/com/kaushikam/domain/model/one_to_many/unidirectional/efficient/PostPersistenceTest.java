package com.kaushikam.domain.model.one_to_many.unidirectional.efficient;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PostPersistenceTest {

    @Test
    public void savePosts() {
        Session session = getSession();
        session.beginTransaction();

        Post post = new Post("First Post");

        post.getComments().add(new PostComment("First review"));
        post.getComments().add(new PostComment("Second review"));
        post.getComments().add(new PostComment("Third review"));
        post.getComments().add(new PostComment("Fourth review"));

        Long id = (Long) session.save(post);
        post.setId(id);

        session.getTransaction().commit();

        Assertions.assertNotNull(post.getId());
        Assertions.assertEquals(1, post.getId());

        session.close();

    }

    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{Post.class, PostComment.class})
                .openSession();
    }
}
