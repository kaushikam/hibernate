package com.kaushikam.domain.model.one_to_many.bidirectional;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PostPersistenceTest {

    @Test
    public void savePost() {
        Session session = getSession();
        session.beginTransaction();

        Post post = new Post("First Post");

        post.addComment(new PostComment("First Review"));
        post.addComment(new PostComment("Second Review"));
        post.addComment(new PostComment("Third Review"));
        post.addComment(new PostComment("Fourth Review"));

        Long id = (Long) session.save(post);
        post.setId(id);

        Assertions.assertEquals(1L, post.getId());

        session.getTransaction().commit();
        session.close();
    }

    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{Post.class, PostComment.class})
                .openSession();
    }
}
