package com.kaushikam.domain.model.one_to_many.unidirectional.efficient;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class PostFindTest {

    @BeforeAll
    public static void setup() {
        Session session = getSession();
        session.beginTransaction();

        Post post = new Post("First Post");

        post.getComments().add(new PostComment("First review"));
        post.getComments().add(new PostComment("Second review"));
        post.getComments().add(new PostComment("Third review"));
        post.getComments().add(new PostComment("Fourth review"));

        Long id = (Long) session.save(post);
        post.setId(id);

        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testFindPost() {
        Session session = getSession();
        session.beginTransaction();

        Post post = session.load(Post.class, 1L);

        Assertions.assertEquals(1L, post.getId());
        Assertions.assertEquals("First Post", post.getTitle());

        session.getTransaction().commit();
        session.close();
    }

    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{Post.class, PostComment.class})
                .openSession();
    }
}
