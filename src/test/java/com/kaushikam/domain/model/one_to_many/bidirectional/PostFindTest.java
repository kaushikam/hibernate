package com.kaushikam.domain.model.one_to_many.bidirectional;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class PostFindTest {
    @BeforeAll
    public static void setup() {
        Session session = getSession();
        session.beginTransaction();

        Post post = new Post("First Post");

        post.addComment(new PostComment("First Review"));
        post.addComment(new PostComment("Second Review"));
        post.addComment(new PostComment("Third Review"));
        post.addComment(new PostComment("Fourth Review"));

        Long id = (Long) session.save(post);

        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testFindPost() {
        Session session = getSession();
        session.beginTransaction();

        Post post = session.load(Post.class, 1L);

        Assertions.assertEquals(1L, post.getId());
        Assertions.assertEquals("First Post", post.getTitle());

        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testRemove() {
        Session session = getSession();
        session.beginTransaction();

        Post post = session.load(Post.class, 1L);
        PostComment comment = post.getComments().get(0);

        post.removeComment(comment);

        session.saveOrUpdate(post);
        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testRemoveCommentDirectly() {
        Session session = getSession();
        session.beginTransaction();

        Post post = session.load(Post.class, 1L);
        PostComment comment = post.getComments().get(0);
        post.getComments().remove(comment);

        session.saveOrUpdate(post);

        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testAddComment() {
        Session session = getSession();
        session.beginTransaction();

        Post post = session.load(Post.class, 1L);
        post.addComment(new PostComment("Fifth Review"));

        session.saveOrUpdate(post);
        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testAddCommentDirectly() {
        Session session = getSession();
        session.beginTransaction();

        Post post = session.load(Post.class, 1L);
        PostComment comment = new PostComment("New comment");
        post.getComments().add(comment);

        session.saveOrUpdate(post);
        session.getTransaction().commit();
        session.close();
    }

    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{Post.class, PostComment.class})
                .openSession();
    }
}
