package com.kaushikam.domain.model.one_to_many.bidirectional;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class PostRemovalTest {

    @BeforeAll
    public static void setup() {
        Session session = getSession();
        session.beginTransaction();

        Post post = new Post("First Post");

        post.addComment(new PostComment("First review"));
        post.addComment(new PostComment("Second review"));
        post.addComment(new PostComment("Third review"));
        post.addComment(new PostComment("Fourth review"));

        Long id = (Long) session.save(post);
        post.setId(id);

        session.getTransaction().commit();

        /*session.beginTransaction();
        System.out.println("Post id is " + post.getId());
        Post savedPost = session.load(Post.class, post.getId());
        savedPost.getComments().forEach(comment -> {
            System.out.println(String.format("ID: %d, Review: %s",
                    comment.getId(), comment.getReview()));
        });*/


        session.close();
    }

    @Test
    public void testDisconnect() {
        Session session = getSession();
        session.beginTransaction();

        Post post = session.get(Post.class, 1L);
        PostComment postComment = post.getComments().get(0);
        displayComments(post);
        post.removeComment(postComment);

        session.getTransaction().commit();
        session.close();
    }



    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{Post.class, PostComment.class})
                .openSession();
    }

    private void displayComments(Post post) {
        post.getComments().forEach(comment -> {
            System.out.println(String.format("ID: %d, Review: %s",
                    comment.getId(), comment.getReview()));
        });
    }
}
