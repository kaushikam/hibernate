package com.kaushikam.domain.model.one_to_many.unidirectional.efficient;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.Session;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class PostRemovalTest {

    @Test
    public void testRemoveComment() {
        Session session = getSession();
        session.beginTransaction();


        Post post = session.load(Post.class, 1L);
        displayComments(post);
        PostComment postComment = post.getComments().get(0);
        post.getComments().remove(postComment);
        session.remove(postComment);
        System.out.println("*********************************************");
        displayComments(post);

        session.getTransaction().commit();
        session.close();
    }

    @BeforeAll
    public static void setup() {
        Session session = getSession();
        session.beginTransaction();

        Post post = new Post("First Post");

        post.getComments().add(new PostComment("First review"));
        post.getComments().add(new PostComment("Second review"));
        post.getComments().add(new PostComment("Third review"));
        post.getComments().add(new PostComment("Fourth review"));

        Long id = (Long) session.save(post);
        post.setId(id);

        session.getTransaction().commit();
        session.close();
    }

    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{Post.class, PostComment.class})
                .openSession();
    }

    private void displayComments(Post post) {
        post.getComments().forEach(comment -> {
            System.out.println(String.format("ID: %d, Review: %s",
                    comment.getId(), comment.getReview()));
        });
    }
}
