package com.kaushikam.domain.model.many_to_many.inefficient;

import com.kaushikam.utils.HibernateCustomUtilities;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class PostFindTest {
    @BeforeAll
    public static void setup() {
        Session session = getSession();
        session.beginTransaction();

        Post post1 = new Post("JPA with Hibernate");
        Post post2 = new Post("Native Hibernate");

        Tag tag1 = new Tag("Java");
        Tag tag2 = new Tag("Hibernate");

        post1.addTag(tag1);
        post1.addTag(tag2);

        post2.addTag(tag1);

        session.save(post1);
        session.save(post2);

        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testFind() {
        Session session = getSession();
        session.beginTransaction();

        Post post = session.load(Post.class, 1L);

        Assertions.assertEquals(1L, post.getId());
        Assertions.assertEquals(2, post.getTags().size());

        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testRemoveTag() {
        Session session = getSession();
        session.beginTransaction();

        Post post = session.get(Post.class, 1L);
        Tag tag = new Tag("Java");
        post.removeTag(tag);

        session.saveOrUpdate(post);

        // session.flush();
        session.getTransaction().commit();
        session.close();
    }


    private static Session getSession() {
        return HibernateCustomUtilities
                .getSessionFactory(new Class[]{Post.class, Tag.class})
                .openSession();
    }
}
