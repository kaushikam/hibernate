package com.kaushikam.domain.model.one_to_one.maps_id_owner_inverse;

import javax.persistence.*;

@Entity
@Table(name = "profile")
public class Profile {

    @Id
    private Long id;

    @Column(name = "email")
    private String email;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    private User user;

    public Profile() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
