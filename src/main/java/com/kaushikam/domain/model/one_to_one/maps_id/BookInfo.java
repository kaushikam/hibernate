package com.kaushikam.domain.model.one_to_one.maps_id;

import javax.persistence.*;

@Entity
@Table(name = "book_info")
public class BookInfo {

    @Id
    private Long id;

    @Column(name = "author")
    private String author;

    /**
     * In this one-to-one relationship, foreign key is stored in this
     * class in the form of primary key. Hence this class is the owner of
     * the relationship.
     */
    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    private Book book;

    public BookInfo() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
