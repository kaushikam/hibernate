package com.kaushikam.domain.model.one_to_one.join_table;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(
        name = "employee_workstation",
        joinColumns = {@JoinColumn(name = "employee_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "work_station_id", referencedColumnName = "id")}
    )
    private WorkStation workStation;

    public Employee() {}

    public Employee(String name) {
        this.name = name;
    }
}
