package com.kaushikam.domain.model.one_to_one.join_table;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "work_station")
public class WorkStation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String floor;

    @OneToOne(mappedBy = "workStation")
    private Employee employee;

    public WorkStation() {}

    public WorkStation(String floor) {
        this.floor = floor;
    }
}
