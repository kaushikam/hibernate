package com.kaushikam.domain.model.one_to_one.join_column;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "post_details")
public class PostDetails {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    private Date createdOn;

    /**
     * The inverse side of a bi directional relationship is referring
     * to its owning side by using the 'mappedBy' element
     *
     * Hence it is the inverse side
     */
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "postDetails")
    private Post post;

    public PostDetails() {}

    public PostDetails(String createdBy) {
        this.createdBy = createdBy;
        this.createdOn = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
