package com.kaushikam.domain.model.one_to_one.join_column;

import javax.persistence.*;

@Entity
@Table(name = "post")
public class Post {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "title")
    private String title;

    /**
     * We have to set cascade to 'CascadeType.ALL' so that
     * the child; which is 'PostDetails' will be persisted first,
     * then the Parent (Post)
    **/

    /**
     * In a One-to-One 'bi directional' relationship, the owning side
     * contains the foreign key. Here the 'Post' class contains the
     * foreign key, hence it is the owning side of the relationship
     */
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "post_details_id")
    private PostDetails postDetails;

    public Post() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PostDetails getPostDetails() {
        return postDetails;
    }

    public void setPostDetails(PostDetails postDetails) {
        if (postDetails == null) {
            if (this.getPostDetails() != null) {
                this.postDetails.setPost(null);
            }
        } else {
            postDetails.setPost(this);
        }

        this.postDetails = postDetails;
    }
}
