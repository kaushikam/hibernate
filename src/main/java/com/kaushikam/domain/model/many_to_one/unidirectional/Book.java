package com.kaushikam.domain.model.many_to_one.unidirectional;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity(name = "Book")
@Table(name = "book")
@NoArgsConstructor
public class Book
        implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;

    private String author;

    @NaturalId
    private String isbn;
}
