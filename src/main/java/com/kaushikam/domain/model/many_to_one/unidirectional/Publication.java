package com.kaushikam.domain.model.many_to_one.unidirectional;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "Publication")
@Table(name = "publication")
public class Publication {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String publisher;

    /**
     * Here Fetch lazy will not work, as there is no way to know
     * the id of the book without loading it.     *
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "isbn",
        referencedColumnName = "isbn"
    )
    private Book book;

    @Column(
            name = "price_in_cents",
            nullable = false
    )
    private Integer priceCents;

    private String currency;
}
