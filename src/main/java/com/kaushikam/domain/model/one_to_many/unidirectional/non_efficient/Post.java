package com.kaushikam.domain.model.one_to_many.unidirectional.non_efficient;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "post")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;

    /**
     * This method is not at all efficient as Hibernate will create
     * three tables 'post', 'post_comment' and 'post_post_comment'.
     * The third table 'post_post_comment' will store the foreign key
     * to both of the tables and the sql will be:
     *
     * create table post_post_comment (
     *        Post_id bigint not null,
     *         comments_id bigint not null
     * )
     *  alter table post_post_comment
     *        add constraint UK_gb03lqukc5jc1u847l17m311q unique (comments_id)
     *  alter table post_post_comment
     *        add constraint FKp3qg7d2x7uh1eiulxe65rhwl2
     *        foreign key (comments_id)
     *        references post_comment
     *  alter table post_post_comment
     *        add constraint FKdylii52824tsuhxvxsnancr51
     *        foreign key (Post_id)
     *        references post
     *
     * Run the test './gradlew clean test --tests *unidirectional.non_efficient.PostPersistenceTest'
     */
    @OneToMany(
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    private List<PostComment> comments = new ArrayList<>();

    public Post() {}

    public Post(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<PostComment> getComments() {
        return comments;
    }

    public void setComments(List<PostComment> comments) {
        this.comments = comments;
    }
}
